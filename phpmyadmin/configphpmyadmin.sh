#!/bin/bash

## Este script inicia y configura el servicio apache con las configuraciones para usar php y mariadb.
# Fichero de configuración /etc/httpd/conf/httpd.conf 


EMAIL='danieldakataca@gmail.com'
FICHEROAPACHE=/etc/httpd/conf/httpd.conf
FICHEROPHP=~/php.ini

## Configuración de Apache Web Server
# ==================  Desarrollo local y correo de administrador =================== #
sudo sed -i \
	-e "s/Listen 80/Listen 127.0.0.1:80/" \
	-e "/ServerAdmin \\w@*.\\w/c\ServerAdmin ${EMAIL}" \
	${FICHEROAPACHE}


# ========================= Fichero /etc/hosts ========================= #
sethosts() {
echo "Configurando fichero /etc/hosts.conf"
sudo tee <<EOF  /etc/hosts >/dev/null
	127.0.0.1 localhost.localdomain localhost
	::1 localhost.localdomain localhost
	127.0.1.1 $HOSTNAME.localdomain $HOSTNAME
EOF
}

if [ -e "/etc/hosts" ]; then
	echo "/etc/hosts existe!"
	sudo sed -i 'd' /etc/hosts
	sethosts	
else
	echo "/etc/hosts No Existe!"
	sudo touch /etc/hosts 
	sethosts
fi


# ========================= Fichero /etc/rc.conf ========================= #
setrc(){
echo "Configurando fichero /etc/rc.conf"
sudo tee <<EOF /etc/rc.conf >/dev/null
	#
	# Networking
	#
	HOSTNAME="localhost"
EOF
}

if [ -e "/etc/rc.conf" ]; then
	echo "/etc/rc.conf existe"
	sudo sed -i 'd' /etc/rc.conf
	setrc
else
	echo "/etc/rc.conf no existe"
	sudo touch /etc/rc.conf
	setrc
fi


# ========================= Gestionar Servicio Apache ========================= #
for option_apache in {1..2}
do
if [[ $option_apache -eq 1  ]]; then
	ini_apache="Iniciar "
	ing_apache="Iniciando "
	action_apache="start"
else
	ini_apache="Habilitar "
	ing_apache="Habilitando "
	action_apache="enable"
fi

PS3="Desea $ini_apache apache?: "
select apache in "si" "no"
do
	case $REPLY in
		1) echo " $ing_apache  apache..."
		echo "sudo systemctl ${action_apache} httpd.service" | bash
		break;;
		2)echo "eligió no $ini_apache apache."
		echo "Recuerde que para $ini_apache apache puede usar:"
		echo "sudo systemctl ${action_apache} httpd.service" | bash
		break;;
		*) echo "Opción incorrecta. Debe elegir 1 o 2, vuelva a interntarlo!"
		;;
	esac
done
done


# Comentar línea en /etc/httpd/conf/httpd.conf
#LoadModule mpm_prefork_module modules/mod_mpm_prefork.so

#sudo sed -i 's/#LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so/LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so/' ${FICHEROAPACHE}


# ========================= Módulos adicionales en LoadModule  ========================= #
# Añadir debajo de las cargas de módulos los comandos para cargar php en apache.
lineload=$(sed -n '/LoadModule/p' ${FICHEROAPACHE} | sed -n '$p' | awk '{print $1 " " $2}')

echo "$endline"

numlineload=$(sed -n "/${lineload}/=" $FICHEROAPACHE)

echo $numlineload

sudo sed -e ${numlineload}a\ "LoadModule php7_module modules\/libphp7.so \nAddHandler php7-script .php" $FICHEROAPACHE


# Añadir debajo de los include comando para incluir php
lineinclude=$(sed -n '/#Include/p' ${FICHEROAPACHE} | sed -n '$p' | cut -d '-' -f2)

echo "$lineinclude"

numlineinclude=$(sed -n "/${lineinclude}/=" $FICHEROAPACHE)

echo "$numlineinclude"

sudo sed -e ${numlineinclude}a\ "Include conf\/extra\/php7_module.conf" $FICHEROAPACHE


# ========================= Reiniciar Servicio Apache ========================= #
PS3="Desea reinicar httpd.service y aplicar la configuración?:"
select rest in "si" "no"
do
	case $REPLY in
		1) echo "Reiniciando httpd.service (apache)."
		sudo systemctl restart httpd.service
		break;;
		2)echo "Los cambios se aplicarán en el próximo reinicio del sistema o de httpd.service (apache)."
		break;;
		*) echo " Opción incorrecta, debe elegir 1 o 2, vuelva a intentarlo"
		;;
	esac
done

## Configuración de mariadb
# preconfiguración
# Generar tabla de mysql.user
sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

# ========================= Gestionar Servicio MariaDB ========================= #
for option_mariadb in {1..2}
do

if [[ $option_mariadb -eq 1 ]]; then
	ini_mariadb="Iniciar "
	ing_mariadb="Iniciando "
	action_mariadb="start"
else
	ini_mariadb="Habilitar  "
	ing_mariadb="Habilitando "
	action_mariadb="enable"

fi

# ========================= Gestionar Servicio MariaDB ========================= #
PS3="Desea  $ini_mariadb el servicio mariadb.service?:"
select mari in "si" "no"
do
	case $REPLY in
		1)echo "$ $ing_mariad bmariadb..."
		sudo systemctl $action_mariadb mariadb.service 
		break;;
		2)echo "Eligió no $ini_mariadb mariadb, en cualquier momento puede hacerlo con:"
		echo "systemctl $ini_mariadb mariadb.service"
		break;;
		*) echo "Opción inválida, debe elegir 1 o 2, vuelva a intentarlo."
		;;
	esac

done
done

sudo sed -i 's/;date.timezone =/date.timezone = America\/Bogota/' $FICHEROPHP

## Descomentar líneas


